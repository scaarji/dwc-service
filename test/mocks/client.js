'use strict';
var sinon = require('sinon');

module.exports = {
    init: sinon.spy(function(callback) {
        process.nextTick(callback);
    }),
    _listener: null,
    listen: sinon.spy(function(info, worker, callback) {
        if (!info) {
            return callback(new Error('No info given'));
        }
        if (!info.name) {
            return callback(new Error('No name given'));
        }
        if (!info.version) {
            return callback(new Error('No version given'));
        }
        if (typeof worker !== 'function') {
            return callback(new Error('Invalid worker given'));
        }
        this._listener = worker;
        process.nextTick(callback);
    }),
    _send: function(payload, callback) {
        this._listener(payload, callback);
    }
};
