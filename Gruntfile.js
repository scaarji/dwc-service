module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: {
            options: {
                jshintrc: true
            },
            all: ['Gruntfile.js', 'index.js', 'lib/**/*.js', 'test/**/*.js']
        },
        mochaTest: {
            options: {
                reporter: 'spec'
            },
            src: ['test/**/*.spec.js']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-mocha-test');

    grunt.registerTask('hint', ['jshint']);
    grunt.registerTask('test', ['mochaTest']);
    grunt.registerTask('default', ['hint', 'test']);
};
