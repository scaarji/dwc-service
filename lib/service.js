'use strict';
var _ = require('lodash'),
    async = require('async');

var Service = function(client, info, handlers) {
    if (!info) {
        throw new Error('Service info can not be empty');
    }

    if (!info.name) {
        throw new Error('Service name can not be empty');
    }

    if (!info.version) {
        throw new Error('Service version can not be empty');
    }

    this.client = client;
    this.logger = console;

    handlers = _.clone(handlers);

    this.handler = function(op) {
        return handlers[op];
    };
    this.handlers = function() {
        return handlers;
    };
    this.info = function() {
        return info;
    };

    var hooks = {};

    _.each(handlers, function(fn, op) {
        hooks[op] = {
            pre: [],
            post: []
        };
    });

    this.hooks = {
        get: function(op, type) {
            if (!_.has(hooks, op)) {
                throw new Error('Invalid operation ' + op);
            }

            if (type !== 'pre' && type !== 'post') {
                throw new Error('Invalid type ' + type);
            }

            return hooks[op][type];
        },
        add: function(op, type, handler) {
            if (!_.has(hooks, op)) {
                throw new Error('Invalid operation ' + op);
            }

            if (type !== 'pre' && type !== 'post') {
                throw new Error('Invalid type ' + type);
            }

            if (_.isArray(handler)) {
                hooks[op][type] = hooks[op][type].concat(handler);
            } else {
                hooks[op][type].push(handler);
            }
        }
    };
};

/**
 * Starts service
 * @param {Function} [callback] Callback to be executed on listen start
 */
Service.prototype.start = function(callback) {
    var _this = this;
    _this.client.init(function() {
        _this.client.listen(_this.info(), _this._route.bind(_this), function(err) {
            if (typeof callback === 'function') {
                callback(err);
            }
        });
    });
};

Service.prototype.register = function(op, handlers) {
    if (typeof op !== 'string') {
        handlers = op;
        op = null;
    }

    var _this = this;
    if (!op) {
        _.each(this.handlers(), function(fn, name) {
            _this.register(name, handlers);
        });
        return;
    }

    if (!this.handler(op)) {
        throw new Error('No handler registered for ' + op);
    }

    if (handlers.pre) {
        this.hooks.add(op, 'pre', handlers.pre);
    }

    if (handlers.post) {
        this.hooks.add(op, 'post', handlers.post);
    }
};

/**
 * Executes corresponding handler if it exists
 * @param {Object} message  Message received from client
 * @param {String} message.op   Name of called operation
 */
Service.prototype._route = function(message, callback) {
    if (typeof message.op !== 'string') {
        return process.nextTick(function() {
            callback(new Error('Invalid operation name'));
        });
    }

    var handler = this.handler(message.op);
    if (typeof handler !== 'function') {
        return process.nextTick(function() {
            callback(new Error('Method ' + message.op + ' not implemented'));
        });
    }

    var _this = this,
        preHooks = this.hooks.get(message.op, 'pre'),
        postHooks = this.hooks.get(message.op, 'post'),
        executeHooks = function(hooks, message, callback) {
            async.reduce(hooks, message, function(message, hook, callback) {
                hook.call(_this, message, callback);
            }, callback);
        },
        packResult = function(callback) {
            return function(err, result) {
                callback(null, {
                    error: err,
                    result: result
                });
            };
        },
        unpackResult = function(callback) {
            return function(err, result) {
                if (err) {
                    callback(err);
                } else {
                    callback(result.error, result.result);
                }
            };
        };

    async.waterfall([
        function(callback) {
            executeHooks(preHooks, message, callback);
        },
        function(message, callback) {
            handler.call(_this, message, packResult(callback));
        },
        function(result, callback) {
            executeHooks(postHooks, result, callback);
        },
    ], unpackResult(callback));
};

module.exports = Service;

