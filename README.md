# Service Registrator

This package includes class to register service and process requests from the bus.

## Usage

Add package as dependency to your package.json file.

Require package in code:

    var Service = require('dwc-service');

Create new service:

    var service = new Service(client, info, handlers);

Where:

- `client` is bus client, that implements methods `init` and `listen`, for example [`dwc-client`](https://bitbucket.org/scaarji/dwc-client)
- `info` is an object containing info about service being registered, it HAS to contain `name` and `version`
- `handlers` is an map that contains pairs `{op: handlerFn}`,
    handler function has to have signature `function(request, callback)` and
    callback should be called with arguments error and result, everything else will be ignored

Add middleware:

service.register(operation, {pre: preHooks, post: postHooks});

Where:

- `operation` name of operation to bind hooks to, can be omitted to bind middleware to all operations
- `preHooks` one function or list of functions with signature `function(message, callback)`, that will be execute in order they were added, if any function returns error, execution is stopped, function has to return message if it was successfully executed
- `postHooks` one function or list of function with signature `function(data, callback)`, where data contains `error` - error from handler and `result` - data from handler, execution is analogous to pre hooks

Start the service (with optional callback):

    service.start([callback])

## To Be Done

- Add concurrency
