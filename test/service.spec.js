/* global describe, it */
'use strict';
var should = require('should'),
    sinon = require('sinon');

var client = require('./mocks/client');

var Service = require('../index');

describe('Service', function() {
    var info = {
        name: 'service',
        version: 1
    };

    describe('#start', function() {
        it('should set listener', function(done) {
            var service = new Service(client, info);
            service.start(function(err) {
                done(err);
            });
        });

        it('should return error on missing op', function(done) {
            var handlers = {
                echo: sinon.spy()
            };
            var service = new Service(client, info, handlers);

            service.start(function() {
                client._send({}, function(err) {
                    should.exist(err);
                    err.message.should.match(/invalid/i);

                    done();
                });
            });
        });

        it('should return error on missing implementation of method', function(done) {
            var handlers = {
                echo: sinon.spy()
            };
            var service = new Service(client, info, handlers);

            service.start(function() {
                client._send({op: 'hello'}, function(err) {
                    should.exist(err);
                    err.message.should.match(/not implemented/i);

                    done();
                });
            });
        });

        it('should use correct handler', function(done) {
            var handlers = {
                echo: sinon.spy(),
                foo: sinon.spy(function(data, callback) {
                    callback(null, null);
                })
            };
            var service = new Service(client, info, handlers);

            service.start(function() {
                client._send({op: 'foo'}, function(err, data) {
                    should.not.exist(err);

                    handlers.echo.called.should.eql(false);
                    handlers.foo.calledOnce.should.eql(true);

                    done();
                });
            });
        });

        it('should call correct hooks', function(done) {
            var input = {
                op: 'foo',
                params: {
                    foo: true,
                    bar: 1
                }
            };
            var output = {
                error: null,
                result: {
                    foo: 'bar'
                }
            };
            var handlers = {
                echo: function() {},
                foo: function(data, callback) {
                    callback(output.error, output.result);
                }
            };
            var preHooks = [
                sinon.spy(function(data, callback) {
                    data.should.eql(input);
                    callback(null, data);
                }),
                sinon.spy(function(data, callback) {
                    data.should.eql(input);
                    callback(null, data);
                }),
                sinon.spy(function(data, callback) {
                    data.should.eql(input);
                    callback(null, data);
                })
            ];
            var postHooks = [
                sinon.spy(function(result, callback) {
                    result.should.eql(output);
                    callback(null, result);
                }),
                sinon.spy(function(result, callback) {
                    result.should.eql(output);
                    callback(null, result);
                }),
                sinon.spy(function(result, callback) {
                    result.should.eql(output);
                    callback(null, result);
                })
            ];

            var service = new Service(client, info, handlers);
            service.register({pre: preHooks[0], post: postHooks[0]});
            service.register('echo', {pre: preHooks[1]});
            service.register('foo', {pre: preHooks[2]});

            service.register('echo', {post: postHooks[1]});
            service.register('foo', {post: [postHooks[2]]});

            service.start(function() {
                client._send(input, function(err, data) {
                    should.not.exist(err);

                    preHooks[0].calledOnce.should.eql(true);
                    preHooks[1].called.should.eql(false);
                    preHooks[2].calledOnce.should.eql(true);

                    postHooks[0].calledOnce.should.eql(true);
                    postHooks[1].called.should.eql(false);
                    postHooks[2].calledOnce.should.eql(true);

                    done();
                });
            });
        });
    });
});
